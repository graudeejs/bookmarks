(function(window) {

  var getJSON = function(url, callback) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'json';
    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        callback(request.response);
      }
    };
    request.send();
  }

  Vue.component('ea', {
    template: '<a :href="href" rel="nofollow noreferrer" target="_blank"><slot></slot><a>',
    props: ['href']
  });

  window.app = new Vue({
    el: document.getElementById('mainContent'),
    data: {
      bookmarks: [],
      search: "",
      searchTags: "",
      activeTags: [],
      loaded: false,
    },
    methods: {
      tagClasses: function(tag) {
        var classes = ['mdl-chip'];
        if (this.activeTags.indexOf(tag) !== -1) {
          classes.push('mdl-chip--active');
        }
        return classes;
      },
      toggleTag: function(tag) {
        var self = this;
        var tagIndex = self.activeTags.indexOf(tag);
        if (tagIndex === -1) {
          self.searchTags = "";
          self.activeTags.push(tag);
        }
        else {
          self.activeTags.splice(tagIndex, 1);
        }
      },
      updateState: function() {
        if (!this.loaded) return;

        var uriSearch = {}

        if (this.search) {
          uriSearch.q = this.search;
        }

        if (this.activeTags.length) {
          uriSearch.t = this.activeTags.join('|');
        }

        var uri = URI(window.location.href);
        uri.removeSearch(['q', 't']);
        if (Object.keys(uriSearch).length) {
          uri.search(uriSearch);
        }
        var uriString = uri.toString();

        if (uriString === window.location.href) {
          return;
        }

        if (uri.protocol() === 'file') {
          console.debug(uriString);
        }
        else {
          history.replaceState(null, null, uriString);
        }
      }
    },
    computed: {
      allTags: function() {
        return _(this.bookmarks).map('tags').flatten().uniq().sortBy().value();
      },
      visibleTags: function() {
        var self = this;
        var result = _(self.allTags);

        if (self.searchTags.length) {
          var valueForRegExp = self.searchTags.replace(/[ ]+/, '.*');
          var rx = new RegExp(valueForRegExp, 'i');
          result = result.filter(function(tag) {
            return tag.match(rx);
          });
        }

        self.updateState();
        return result.value();
      },
      visibleBookmarks: function() {
        var self = this;
        var result = _(self.bookmarks);

        if (self.search.length) {
          var valueForRegExp = self.search.replace(/[ ]+/, '.*');
          var rx = new RegExp(valueForRegExp, 'i');
          result = result.filter(function(bookmark) {
            return bookmark.title.match(rx);
          });
        }

        if (self.activeTags.length) {
          var tags = self.activeTags;
          var withTags = _.filter(tags, function(tag) {
            return tag[0] !== "!";
          });
          var withoutTags = _(tags).filter(function(tag) {
            return tag[0] == "!";
          }).map(function(tag) {
            return tag.substr(1);
          }).value();

          result = result.filter(function(bookmark) {
            for (var i = 0; i < withTags.length; i++) {
              var tag = withTags[i];
              if (bookmark.tags.indexOf(tag) === -1) return false;
            }

            for (var i = 0; i < withoutTags.length; i++) {
              var tag = withoutTags[i];
              if (bookmark.tags.indexOf(tag) !== -1) return false;
            }

            return true;
          });
        }

        self.updateState();
        return result.value();
      }
    },
    mounted: function() {
      var self = this;
      getJSON('bookmarks.json', function(response) {
        self.bookmarks = response;
        var uri = URI.parse(window.location.href);
        var query = URI.parseQuery(uri.query);
        if (query.q) {
          self.search = query.q;
        }
        if (query.t) {
          self.activeTags = _.filter(query.t.split('|'), 'length');
        }
        self.loaded = true;
      });
    }
  });

})(self);
